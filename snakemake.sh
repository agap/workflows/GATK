#!/bin/bash 
#
#SBATCH -J logs/GATK
#SBATCH -o logs/GATK."%j".out
#SBATCH -e logs/GATK."%j".err 
 
# Partition name
#SBATCH -p agap_long

module purge

module load snakemake/7.15.1-conda
module load singularity/3.5
 
mkdir -p logs/ 

### Snakemake commands
# Dry run
if [ "$1" = "dry" ]
then
    snakemake --profile profile  --jobs 200 --printshellcmds --dryrun --use-singularity --cores 1
elif [ "$1" = "unlock" ]
then
    # Unlock repository if one job failed
    snakemake   --profile profile --jobs 200 --unlock  --use-singularity
elif [ "$1" = "dag" ]
then
    # Create DAG file
    snakemake  --profile profile --jobs 2 --dag  --use-singularity | dot -Tpng > dag.png
else
    # Run workflow
    snakemake --profile profile --jobs 60 --cores 140 -p --use-singularity --latency-wait 120
fi

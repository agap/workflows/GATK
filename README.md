# Snakemake workflow: SNP Calling using GATK4

[![Snakemake](https://img.shields.io/badge/snakemake-≥6.3.0-brightgreen.svg)](https://snakemake.github.io)
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](http://www.gnu.org/licenses/gpl.html)       

**Table of Contents** 

  - [Objective](#objective)
  - [Dependencies](#dependencies)
  - [Overview of programmed workflow](#overview-of-programmed-workflow)
  - [Procedure](#procedure)   

## Objective

SNP Calling using GATK4 with GenomicDBImport

## Dependencies

* fastp : A tool designed to provide fast all-in-one preprocessing for FastQ files. This tool is developed in C++ with multithreading supported to afford high performance (https://github.com/OpenGene/fastp)
* bwa : BWA is a software package for mapping low-divergent sequences against a large reference genome (http://bio-bwa.sourceforge.net/)
* samtools : Reading/writing/editing/indexing/viewing SAM/BAM/CRAM format (http://www.htslib.org/)
* sambamba : process your BAM data faster! (https://lomereiter.github.io/sambamba/index.html)
* picard : A set of command line tools (in Java) for manipulating high-throughput sequencing (HTS) data and formats such as SAM/BAM/CRAM and VCF. (https://broadinstitute.github.io/picard/)
* GATK : Variant Discovery in High-Throughput Sequencing Data (https://gatk.broadinstitute.org/hc/en-us) 

## Overview of programmed workflow

<p align="center">
<img src="images/dag.png" width="50%">
</p>

## Procedure 

- Clone the code repository from github to your computer by running the
  following shell command in your terminal:
  ```
  git clone https://gitlab.cirad.fr/agap/cluster/snakemake/GATK.git
  ```


- Change directories into the cloned repository:
  ```
  cd GATK
  ```

- Edit metadata.tsv file according to your need. 

Tab file with the sample name, prefix and the path to R1 and R2

* Example

|sample  |prefix  |fq1  |fq2  |
|--|--|--|--|
|SRR6188453 | SRR6188453  |/storage/replicated/cirad/projects/SEAPAG/wgs/wu2018/raw_data/SRR6188453_1.fastq.gz  |/storage/replicated/cirad/projects/SEAPAG/wgs/wu2018/raw_data/SRR6188453_2.fastq.gz |


- Edit the configuration file config.yaml

Update reference param

```bash
reference: "/storage/replicated/cirad/web/HUBs/citrus/citrus_maxima/CITMA.fasta"
```

- Print shell commands to validate your modification

```
./snakemake dry
```

- Run workflow on Meso@LR

```bash
sbatch snakemake.sh 
```

- Unlock directory

```bash
./snakemake.sh unlock
```

- Create DAG file from workflow

```bash
./snakemake.sh dag
```


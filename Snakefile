#!/usr/bin/env python
import pandas as pd

import os,sys
 
configfile: "config.yaml"

reference=config["reference"]
scaffolds = [] 
with open(reference,'rt') as fh:
    for line in fh:
        line = line.strip()
        if line.startswith(">"):
            line = line.split(" ")[0]
            scaffolds.append(line[1:])

samples = pd.read_csv("metadata.tsv",sep='\t').set_index(["prefix"], drop=False)  
shell("mkdir -p tmpdir")

wildcard_constraints:
    prefix = "|".join(samples.index),
    sample = "|".join(samples["sample"]),
    scaffold = "|".join(scaffolds)

def sample_is_single_end(sample):
    """This function detect missing value in the column 2 of the units.tsv"""
    if "fq2" not in samples.columns:
        return True
    else:
        return pd.isnull(samples.loc[(sample), "fq2"])

def get_fastq(wildcards):
    """This function checks if the sample has paired end or single end reads and returns 1 or 2 names of the fastq files"""
    if sample_is_single_end(wildcards.sample):
        return samples.loc[(wildcards.sample), ["fq1"]].dropna()
    else:
        return samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()


def get_fastp_names(wildcards):
    """
    This function:
      1. Checks if the sample is paired end or single end
      2. Returns the correct input and output trimmed file names. 
    """
    if sample_is_single_end(wildcards.sample):
        inFile = samples.loc[(wildcards.sample), ["fq1"]].dropna() 
        return "--in1 " + inFile[0] + " --out1 fastp/" + wildcards.sample + "_R1.fastq.gz "

    else:
        inFile = samples.loc[(wildcards.sample), ["fq1", "fq2"]].dropna()
        return "--in1 " + inFile[0] + " --in2 " + inFile[1] + " --out1 fastp/" + wildcards.sample + "_R1.fastq.gz --out2 fastp/"+ wildcards.sample + "_R2.fastq.gz"

def get_bwa_names(wildcards):
    """
    This function:
      1. Checks if the sample is paired end or single end.
      2. Returns the correct input file names for STAR mapping step.
    """
    if sample_is_single_end(wildcards.sample):
        return "fastp/" + wildcards.sample + "_R1.fastq.gz"     
    else:
        return "fastp/" + wildcards.sample + "_R1.fastq.gz " + "fastp/" + wildcards.sample + "_R2.fastq.gz"

def get_haplotype_caller_input(wildcards):
    if config["gbs"]!=0:
        return rules.SortSam.output.bam 
    elif config["skip_recalibration"] != 0:
        return rules.sambamba_markdup.output.bam
    else:
        return rules.ApplyBQSR.output.bam

def get_mem_bwa_mem2(wildcards, threads):
    return threads * 4096

###Can be given to increase mem on each crash
def get_mem_mb(wildcards, attempt):
    if(attempt == 1):
        return 10000
    elif(attempt == 2):
        return 20000
    else:
        return 40000


rule all:
    input:
        config["reference"]+".fai", 
        expand("gatk/{sample}_{scaffold}.g.vcf.gz",sample=samples.index,scaffold=scaffolds),  
        "vcf/all_raw.vcf.gz",
        "vcf/all_filtered_indel.vcf.gz",
        "vcf/all_filtered_snp.vcf.gz"

rule mapping_only:
    input:
        bam = expand("bwa/{sample}" + "_rmdup.bam",sample=samples.index), 
      #  metrics = expand("sambamba/{sample}" + ".metrics",sample=samples.index)


rule CreateSequenceDictionary:
    input:
        reference=config["reference"]
    output:
        dict=os.path.splitext(config["reference"])[0]+".dict"
    envmodules:
        config["modules"]["picard"]
    singularity:
        config["container"]["picard"]
    shell:""" 
        picard CreateSequenceDictionary R={input.reference} O={output.dict}  
    """

rule faidx:
    input:
        reference=config["reference"]
    output:
        fai=config["reference"]+".fai"
    envmodules:
        config["modules"]["samtools"]
    singularity:
        config["container"]["samtools"]    
    shell:""" 
        samtools faidx {input.reference}
    """ 

rule bwa_index:
    input:
        reference=config["reference"]
    output:
        bwt=config["reference"]+".bwt.2bit.64"
    envmodules:
        config["modules"]["bwa-mem2"]
    singularity:
        config["container"]["bwa-mem2"]  
    shell:""" 
        bwa-mem2 index {input.reference}  
    """
        

rule fastp:
    input:
        get_fastq      
    output:
        r1 = temp("fastp/{sample}_R1.fastq.gz"),
        r2 = temp("fastp/{sample}_R2.fastq.gz"), 
        json = "fastp/{sample}.json",
        html = "fastp/{sample}.html"
    message:"trimming {wildcards.sample} reads"
    params:
        sample = "{sample}",
        in_and_out_files =  get_fastp_names
    threads: 
        config["threads"]["fastp"]
    envmodules:
        config["modules"]["fastp"]
    singularity:
        config["container"]["fastp"]  
    shell:"""
        touch {output.r2};  
        fastp --thread {threads} {params.in_and_out_files} --json {output.json} --html {output.html}
    """
 
rule bwa:
    input:
        r1 = rules.fastp.output.r1,
        r2 = rules.fastp.output.r2,
        reference=config["reference"],
        bwt = rules.bwa_index.output.bwt,
        fai = rules.faidx.output.fai,
        dict=rules.CreateSequenceDictionary.output.dict
    output:
        sam = temp("bwa/{sample}" + ".sam")
    params: 
        sample_name =  "{sample}",
        read_group=r"-R '@RG\tID:{sample}\tLB:{sample}\tPL:ILLUMINA\tPM:HISEQ\tSM:{sample}'",
        input_file_names =  get_bwa_names
    threads: 
        config["threads"]["bwa"]
    resources:
        mem_mb=get_mem_bwa_mem2
    envmodules:
        config["modules"]["bwa-mem2"]
    singularity:
        config["container"]["bwa-mem2"]
    shell:""" 
        bwa-mem2 mem -t {threads} -K 100000000 -Y {params.read_group} {input.reference} {params.input_file_names} > {output.sam}
    """
  
rule sambamba_view:
    input:
        rules.bwa.output.sam
    output:
        bam = temp("bwa/{sample}" + ".unsorted.bam")
    envmodules:
        config["modules"]["sambamba"]
    singularity:
        config["container"]["sambamba"]  
    threads:
        config["threads"]["sambamba"]
    shell:""" 
        sambamba view -S --format=bam -h -t {threads}  -F "mapping_quality>=20 and proper_pair" -o {output.bam} {input} 
    """
rule sambamba_sort:
    input:
        rules.sambamba_view.output.bam
    output:
        bam = temp("bwa/{sample}" + ".bam")
    envmodules:
        config["modules"]["sambamba"]
    resources:
        mem_mb=get_mem_mb
    singularity:
        config["container"]["sambamba"]  
    threads:
        config["threads"]["sambamba"]
    shell:""" 
        sambamba sort -t {threads} --tmpdir=`pwd`/tmpdir -o {output.bam} {input} 
    """

rule sambamba_index:
    input:
        rules.sambamba_sort.output.bam
    output:
        bai = temp("bwa/{sample}" + ".bam.bai")
    envmodules:
        config["modules"]["sambamba"]
    singularity:
        config["container"]["sambamba"] 
    threads:
        config["threads"]["sambamba"] 
    shell:""" 
        sambamba index -t {threads} {input}
    """
        
rule sambamba_markdup:
    input:
        bam = rules.sambamba_sort.output.bam    
    output:
        bam = "bwa/{sample}" + "_rmdup.bam",
        metrics = "bwa/{sample}" + ".metrics" 
    envmodules:
        config["modules"]["sambamba"]
    resources:
        mem_mb=get_mem_mb
    singularity:
        config["container"]["sambamba"]   
    threads:
        config["threads"]["sambamba"] 
    shell:""" 
        sambamba markdup --tmpdir=`pwd`/tmpdir -t {threads} -r {input} {output.bam} 2> {output.metrics} 
    """
# 

rule HaplotypeCaller0:
    input:
        bam = rules.sambamba_markdup.output.bam,
        reference=config["reference"]
    output:
        vcf = "gatk/{sample}" + "_first.vcf"
    threads: 
        config["threads"]["gatk"]
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk --java-options "-Xmx{resources.mem_mb}M" HaplotypeCaller --output-mode EMIT_VARIANTS_ONLY  --native-pair-hmm-threads {threads} --dont-use-soft-clipped-bases -R {input.reference} -I {input.bam} -O {output.vcf}
    """
 
    
rule SelectVariantsSNP:
    input:
        known_variant = rules.HaplotypeCaller0.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_snp_selected.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants -R {input.reference} -V {input.known_variant} -O {output.vcf}  --select-type-to-include SNP
    """
          
rule VariantFiltrationSNP:
    input:
        vcf = rules.SelectVariantsSNP.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_snp_filtered.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk VariantFiltration \
            -filter "QD < 2.0" --filter-name "QD2" \
            -filter "QUAL < 30.0" --filter-name "QUAL30" \
            -filter "SOR > 3.0" --filter-name "SOR3" \
            -filter "FS > 60.0" --filter-name "FS60" \
            -filter "MQ < 40.0" --filter-name "MQ40" \
            -filter "MQRankSum < -12.5" --filter-name "MQRankSum-12.5" \
            -filter "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum-8" \
            -V {input.vcf} -R {input.reference} -O {output.vcf}
    """
    
rule ExcludeSNP_Filtered:
    input:
        vcf = rules.VariantFiltrationSNP.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_snp_bqsr.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants --exclude-filtered -V {input.vcf} -O {output.vcf}
    """
    
rule SelectVariantsINDEL:
    input:
        known_variant = rules.HaplotypeCaller0.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_indel_selected.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants -R {input.reference} -V {input.known_variant} -O {output.vcf}  --select-type-to-include INDEL
    """
    
rule VariantFiltrationINDEL:
    input:
        vcf = rules.SelectVariantsINDEL.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_indel_filtered.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk VariantFiltration \
            -filter "QD < 2.0" --filter-name "QD2" \
            -filter "QUAL < 30.0" --filter-name "QUAL30" \
            -filter "FS > 200.0" --filter-name "FS200" \
            -filter "ReadPosRankSum < -20.0" --filter-name "ReadPosRankSum-20" \
            -V {input.vcf} -R {input.reference} -O {output.vcf}
    """
     
rule ExcludeINDEL_Filtered:
    input:
        vcf = rules.VariantFiltrationINDEL.output.vcf,
        reference=config["reference"]
    output:
        vcf =  "gatk/{sample}" + "_indel_bqsr.vcf"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants --exclude-filtered -V {input.vcf} -O {output.vcf}
    """
rule BaseRecalibrator:
    input:
        bam = rules.sambamba_markdup.output.bam,
        vcf_indel = config["known_sites"] if config["known_sites"] else rules.ExcludeINDEL_Filtered.output.vcf,
        vcf_snp = config["known_sites"] if config["known_sites"] else rules.ExcludeSNP_Filtered.output.vcf,
        reference=config["reference"]
    output:
        recal = "gatk/{sample}" + "_BR_recal.grp"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk --java-options "-Xmx{resources.mem_mb}M" BaseRecalibrator -I {input.bam} -R {input.reference}  --known-sites {input.vcf_indel}  --known-sites {input.vcf_snp} -O {output.recal}
    """
rule ApplyBQSR:
    input:
        bam = rules.sambamba_markdup.output.bam,
        recal = rules.BaseRecalibrator.output.recal,
        reference=config["reference"]
    output:
        bam =  "gatk/{sample}" + "_recalibrate.bam",
        index =  "gatk/{sample}" + "_recalibrate.bai"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk --java-options "-Xmx{resources.mem_mb}M" ApplyBQSR -I {input.bam} -R  {input.reference}  --bqsr-recal-file {input.recal} -O {output.bam}
    """

rule sambamba_flagstat:
    input:
        bam = rules.ApplyBQSR.output.bam        
    output:
        "gatk/{sample}.flagstat"    
    envmodules:
        config["modules"]["sambamba"] 
    singularity:
        config["container"]["sambamba"]  
    threads:
        config["threads"]["sambamba"]
    shell:"""
        sambamba flagstat -t {threads} {input.bam} > {output} 
    """
 
 
rule HaplotypeCaller:
    input:
        bam = get_haplotype_caller_input,
        bai = rules.sambamba_index.output.bai,    
        reference=config["reference"]
    
    output:
        gvcf =  "gatk/{sample}_{scaffold}" + ".g.vcf.gz",
        tbi =  "gatk/{sample}_{scaffold}" + ".g.vcf.gz.tbi"
    threads: 
        config["threads"]["gatk"]
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:"""    
        gatk --java-options "-Xmx{resources.mem_mb}M" HaplotypeCaller --native-pair-hmm-threads {threads} -R {input.reference} -I {input.bam} -O {output.gvcf}  --L {wildcards.scaffold}  -ERC GVCF
    """

rule createSampleMap:
    input:
        gvcfs=expand("gatk/{sample}_{scaffold}.g.vcf.gz.tbi", zip,sample=samples.index,scaffold=scaffolds)
    output:
        temp("{scaffold}.tsv")
    params:
        "{scaffold}"
    run:
        with open(output[0], "w") as sample_map:
            for sample in samples.index:
                sample_map.write(f"{sample}\tgatk/{sample}_"+params[0]+".g.vcf.gz\n")
            sample_map.close()
        


rule GenomicsDBImport:
    input:"{scaffold}.tsv"
    output:
        db = directory("genomicsDB_{scaffold}/{scaffold}.db")
    threads: 
        config["threads"]["gatk"]
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:"""
        gatk --java-options '-Xmx{resources.mem_mb}M' GenomicsDBImport --L {wildcards.scaffold}  --sample-name-map {input}  --genomicsdb-workspace-path {output.db} --reader-threads {threads}  --batch-size 50
    """

rule GenotypeGVCFs:
    input:
        db="genomicsDB_{scaffold}/{scaffold}.db",
        reference = config["reference"]
    output:
        "vcf/{scaffold}.vcf.gz"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk --java-options '-Xmx{resources.mem_mb}M -Xms{resources.mem_mb}M ' GenotypeGVCFs  -R {input.reference} -O {output} --genomicsdb-use-bcf-codec --only-output-calls-starting-in-intervals -V gendb://{input.db}  -L {wildcards.scaffold}
    """

rule write_list:
    input:
        expand("vcf/{scaffold}.vcf.gz",scaffold = scaffolds)
    output:
        file_list = temp("list.txt")
    shell:
        "ls {input} > {output.file_list}"


rule MergeVcfs:
    input:
        expand("vcf/{scaffold}.vcf.gz",scaffold = scaffolds)
    output:
        vcf="vcf/all_raw.vcf.gz" 
    envmodules:
        config["modules"]["bcftools"]
    singularity:
        config["container"]["bcftools"]  
    threads:
        config["threads"]["sambamba"]
    shell:""" 
        bcftools concat --threads {threads} {input} -o {output.vcf}
    """
     
#rule MergeVcfs:
#    input:
#        file_list = rules.write_list.output.file_list
#    output:
#        vcf="vcf/all_raw.vcf.gz" 
#    envmodules:
#        config["modules"]["picard"]
#    singularity:
#        config["container"]["picard"]  
#    shell:""" 
#        picard  -Xmx{resources.mem_mb}M  MergeVcfs I={input.file_list} O={output.vcf}
#    """
     

rule selectSNPs:
    input:
        vcf = rules.MergeVcfs.output.vcf
    output:
        vcf = "vcf/all_raw_snp.vcf.gz"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants \
        -V {input.vcf} \
        -select-type SNP \
        -O {output.vcf}
    """
rule selectINDELs:
    input:
        vcf = rules.MergeVcfs.output.vcf
    output:
        vcf = "vcf/all_raw_indel.vcf.gz"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk SelectVariants \
        -V {input.vcf} \
        -select-type INDEL \
        -O {output.vcf}
    """

rule hardfilterSNPs:
    input:
        vcf = rules.selectSNPs.output.vcf
    output:
        vcf = "vcf/all_filtered_snp.vcf.gz"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk VariantFiltration \
        -V {input.vcf} \
        -filter "QD < 2.0" --filter-name "QD2" \
        -filter "QUAL < 30.0" --filter-name "QUAL30" \
        -filter "SOR > 3.0" --filter-name "SOR3" \
        -filter "FS > 60.0" --filter-name "FS60" \
        -filter "MQ < 40.0" --filter-name "MQ40" \
        -filter "MQRankSum < -12.5" --filter-name "MQRankSum-12.5" \
        -filter "ReadPosRankSum < -8.0" --filter-name "ReadPosRankSum-8" \
        -O {output.vcf}
    """

rule hardfilterINDELs:
    input:
        vcf = rules.selectINDELs.output.vcf
    output:
        vcf = "vcf/all_filtered_indel.vcf.gz"
    envmodules:
        config["modules"]["gatk"]
    singularity:
        config["container"]["gatk"]  
    shell:""" 
        gatk VariantFiltration \
        -V {input.vcf} \
        -filter "QD < 2.0" --filter-name "QD2" \
        -filter "QUAL < 30.0" --filter-name "QUAL30" \
        -filter "FS > 200.0" --filter-name "FS200" \
        -filter "ReadPosRankSum < -20.0" --filter-name "ReadPosRankSum-20" \
        -O {output.vcf}
    """
